
//--------------------------Classes------------------------------
class Animal {
    constructor(name) {
      this.name = name;
    }
    speak() {
      console.log(this.name + ' makes a noise.');
    }
  }
  
  const animal = new Animal('animal');
  animal.speak(); // animal makes a noise.

  //------------------------------Inheritance-----------------------------
  class Lion extends Animal
  {
      speak()
      {
          super.speak();
          console.log(`${this.name} roars`);
      }
  }
  const lion = new Lion("Simba");
  lion.speak();

