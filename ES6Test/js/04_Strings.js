
//-----------------------Template Literals------------------
//Не нужно больше делать вложенную конкатенацию, можно использовать шаблоны
const name = "Polina";
const lastName = "Liolia";
console.log(`Name: ${name}, last name: ${lastName}`); //!!! косые кавычки !!!


//---------------------------Multi-line strings-----------------------
//Не нужно больше конкатенировать строки с + \n
//!!! косые кавычки !!!
const multiLine = `this 
is 
multi-line
text`;
console.log(multiLine);