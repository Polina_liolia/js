 //---------------------------Promises------------------------------
 //Код, которому надо сделать что-то асинхронно, создаёт объект promise и возвращает его.
 function printAfterTimeout(string, timeout){
    return new Promise((resolve, reject) => {
      setTimeout(function(){
        resolve(string);
      }, timeout);
    });
  }
  
  //Внешний код, получив promise, навешивает на него обработчики.
  printAfterTimeout('Hello ', 2e3).then((result) => {
    console.log(result);
    return printAfterTimeout(result + 'Reader', 2e3); //создаёт объект promise и возвращает его.
  
  }).then((result) => { //навешивает на него обработчики.
    console.log(result);
  });
  //По завершении процесса асинхронный код переводит promise в состояние fulfilled (с результатом) 
  //или rejected (с ошибкой). При этом автоматически вызываются соответствующие обработчики во внешнем коде.

  //---------------Стрелочные функции---------------------
// создаём каждому щенку по пустому объекту в качестве игрушки
var chewToys = puppies.map(puppy => {});   // БАГ!
var chewToys = puppies.map(puppy => ({})); // всё хорошо

//У стрелочных функций нет собственного значения this. Внутри стрелочной функции this 
//всегда наследуется из окружающего лексического окружения.
//стрелочные функции не получают собственного объекта arguments