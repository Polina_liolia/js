//---------------Асинхронные функции---------------

//Для создания асинхронной функции используется ключевое слово async.

//Объявление асинхронной функции: 
async function asyncFunc1() {}

//Выражение с асинхронной функцией: 
const asyncFunc2 = async function () {};

//Метод с асинхронной функцией: 
let obj3 = { 
    async asyncFunc() { } 
}

//Стрелочная асинхронная функция: 
const asyncFunc4 = async () => {};



//асинхронная функция
async function howOldAreYou()
{
    console.log("Async function goes...");
    return 28;
}

console.log("Начало");
//вызов этой ф-ции вернет не число 28, а Promise
let asyncResult = howOldAreYou()
.then(
    result => console.log(`Result: ${result}`),
    error => colnsole.log(`Error: ${error}`));
console.log(`Конец: Async functions always return ${asyncResult instanceof Promise ? "Promise" : "something"}`);


//использование await
function rand(min, max) {
 return Math.floor(Math.random() * (max - min + 1)) + min;
}
async function randomMessage() {
 const message = [
   'Привет',
   'Куда пропал?',
   'Давно не виделись'
 ][rand(0, 2)];
 return timeout(message, 5);
}
async function chat() {
 const message = await randomMessage(); //дожидаемся выполнения асинхронной функции
 console.log(message);
}
console.log('Начало');
chat();
console.log('Конец');

