//-------------------Остаточные параметры---------------------
function containsAll(haystack, ...needles) {
    for (var needle of needles) {
      if (haystack.indexOf(needle) === -1) {
        return false;
      }
    }
    return true;
  }
  
  
  //-------------------Spread operation-------------------------
  //от apply() к spread:
  //ES5
  Math.max.apply(Math, [2,100,1,6,43]) // 100
  
  //ES6
  Math.max(...[2,100,1,6,43]) // 100
  
  // от concat к spread:
  //ES5
  var array11 = [2,100,1,6,43];
  var array22 = ['a', 'b', 'c', 'd'];
  var array33 = [false, true, null, undefined];
  console.log(array1.concat(array2, array3));
  
  //ES6
  const array1 = [2,100,1,6,43];
  const array2 = ['a', 'b', 'c', 'd'];
  const array3 = [false, true, null, undefined];
  console.log([...array1, ...array2, ...array3]);
  