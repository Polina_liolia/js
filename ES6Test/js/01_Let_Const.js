//---------------let and const------------------
const a = 15;
//a = 30; // error Assignment to constant variable.
console.log(a);

const arr = [];
arr[0] = 0;
arr[1] = 1;
arr[2] = 2;
//arr = [];//error
console.log(arr); //0, 1, 2

const obj = {};
obj.prop1 = "prop1";
obj.prop2 = 2;
//obj = {prop1: "prop1", prop2: 2}; //error
console.log(obj);

const date = Date();
//date = Date(); //error
console.log(date);

//-------------------blocks and let----------------
let b = 1; {
    let b = 2;
    console.log(b); //2
}
console.log(b); //1
