//----------------------Destructuring Assignment-----------------
const array = [1, 2, 3, 4];
//Получение элемента из массива
//some kind of variables array:
const [first, second, , fourth] = array;
console.log(first, second, fourth);

//-------------------------Обмен значениями---------------------
let var1 = 1;
let var2 = 2;
[var1, var2] = [var2, var1];
console.log(var1, var2);

//---------------Деструктуризация нескольких возвращаемых значений-----------
function margin ()
{
    let top1 = 1, right = 2, bottom = 3, left = 4;
    return { top1, right, bottom, left };
}
//getting part of data from function:
//here are two constants - top and bottom
const {top1, bottom} = margin();
console.log(top1, bottom);
console.log("hi");

//---------------------------Глубокое сопоставление (object destructing).-------------------
function getSomeData()
{
    return {display: {color: "red", margin: 5}, view: {name: "myview", order: 8}};
}

const {display: {color: myColor, margin: myMargin}, view: {name: myViewName, order: myOrder}} = getSomeData();
console.log(myColor, myMargin, myViewName, myOrder);