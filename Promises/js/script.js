'use strict'

// Создаётся объект promise
let promise = new Promise((resolve, reject) => {
    let rand = Math.random() * 100;
    setTimeout(() => {
        if (rand < 50) {
            // переведёт промис в состояние fulfilled с результатом "result"
            resolve(`Bingo! We've got such random number: ${rand}.`);
        } else {
            // переведёт промис в состояние rejected с результатом "result"
            reject(`Oooops! The number is ${rand}.`);
        }
    }, 3000);
}).then( //then навешивает обработчики на успешный результат или ошибку
    (result) => { // первая функция-обработчик - запустится при вызове resolve
        console.log(result);
    },
    (error) => { // вторая функция - запустится при вызове reject
        console.log(error);
    });

//Промисификация
//Промисификация – это когда берут асинхронный функционал и делают для него обёртку, возвращающую промис.
function httpGet(url) {

    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);

        xhr.onload = function () {
            if (this.status == 200) {
                resolve(this.response);
            } else {
                var error = new Error(this.statusText);
                error.code = this.status;
                reject(error);
            }
        };

        xhr.onerror = function () {
            reject(new Error("Network Error"));
        };

        xhr.send();
    });
}
/*
Цепочки промисов
«Чейнинг» (chaining), то есть возможность строить асинхронные цепочки из промисов.

Например, мы хотим по очереди:
Загрузить данные посетителя с сервера (асинхронно).
Затем отправить запрос о нём на github (асинхронно).
Когда это будет готово, вывести его github-аватар на экран (асинхронно).
…И сделать код расширяемым, чтобы цепочку можно было легко продолжить.
 */

//Вот код для этого, использующий функцию httpGet, описанную выше:
// сделать запрос
httpGet('/article/promise/user.json')
    // 1. Получить данные о пользователе в JSON и передать дальше
    .then(response => {
        console.log(response);
        let user = JSON.parse(response);
        return user;
    })
    // 2. Получить информацию с github
    .then(user => {
        console.log(user);
        return httpGet(`https://api.github.com/users/${user.name}`); //тут возвращается промис
    })
    // 3. Вывести аватар на 3 секунды (можно с анимацией)
    .then(githubUser => {
        console.log(githubUser);
        githubUser = JSON.parse(githubUser);

        let img = new Image();
        img.src = githubUser.avatar_url;
        img.className = "promise-avatar-example";
        document.body.appendChild(img);

        setTimeout(() => img.remove(), 3000); // (*)
    })
    .catch(error => {
        console.log(error); // Error: Not Found
    });;

//Promise.all(iterable) 
//получает массив (или другой итерируемый объект) промисов и возвращает промис,
// который ждёт, пока все переданные промисы завершатся, и переходит в состояние «выполнено»
// с массивом их результатов.

Promise.all([
        httpGet('/article/promise/user.json'),
        httpGet('/article/promise/guest.json')
    ]).then(results => {
        console.log(results);
    })
    .catch(error => {
        console.log(error);
    });

//Promise.race, как и Promise.all, получает итерируемый объект с промисами, которые нужно выполнить,
// и возвращает новый промис.
//Но, в отличие от Promise.all, результатом будет только первый успешно выполнившийся промис из списка. 
//Остальные игнорируются.
Promise.race([
    httpGet('/article/promise/user.json'),
    httpGet('/article/promise/guest.json')
]).then(firstResult => {
    firstResult = JSON.parse(firstResult);
    console.log()(firstResult.name); // iliakan или guest, смотря что загрузится раньше
}).catch(error => {
    console.log(error);
});

//Promise.resolve(value) создаёт успешно выполнившийся промис с результатом value.
Promise.resolve(window.location) // начать с этого значения
    .then(httpGet) // вызвать для него httpGet
    .then(console.log) // и вывести результат

//Аналогично Promise.resolve(value) создаёт уже выполнившийся промис,
// но не с успешным результатом, а с ошибкой error.
Promise.reject(new Error("..."))
    .catch(console.log) // Error: ...